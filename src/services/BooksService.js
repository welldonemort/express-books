const fs = require('fs');
const util = require('util');
const path = require('path');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const BookshelvesService = require('./BookshelvesService');

const bookshelvesService = new BookshelvesService(path.join(__dirname, '../data/bookshelves.json'));

/**
 * Logic for reading and writing data
 */
class BooksService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSON file that contains the data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  /**
   * Fetches data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, 'utf8');
    if (!data) return [];
    return JSON.parse(data);
  }

  /**
   * Get all items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Add a new item
   * @param {*} book The book object
   */
  async addEntry(book) {
    const data = (await this.getData()) || [];
    let lastIndex = null;
    let idCounted = null;
    if (data.length) {
      lastIndex = data.length - 1;
      idCounted = data[lastIndex].id + 1;
    } else {
      idCounted = 1;
    }

    const standardParams = {
      id: idCounted,
      status: 'free',
      readerName: null,
      returnDate: null,
      reservedBy: null,
    };

    data.push({ ...standardParams, ...book });
    await bookshelvesService.putBook(book.bookshelfId, idCounted);

    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Get book by ID
   * @param {*} id The ID of a book
   */
  async getEntry(id) {
    const data = (await this.getData()) || [];
    const book = data.find((b) => b.id === id);

    return book;
  }

  /**
   * Change status
   * @param {*} id The ID of the item
   * @param {*} bookshelfId The ID of the bookshelf to place the book
   */
  async changeBookshelf(id, bookshelfId) {
    const data = (await this.getData()) || [];
    const book = data.find((b) => b.id === id);

    await bookshelvesService.removeBook(book.bookshelfId, id);

    if (book) {
      book.bookshelfId = bookshelfId;
    }

    await bookshelvesService.putBook(bookshelfId, id);

    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Change info
   * @param {*} id An ID of the item
   * @param {*} status Status of the item
   * @param {*} readerName The name of a reader
   * @param {*} returnDate The date a reader will return a book
   */
  async updateBook({ id, status, readerName, returnDate }) {
    const data = (await this.getData()) || [];
    const book = data.find((b) => b.id === id);
    if (book) {
      book.status = status;

      if (status === 'taken') {
        book.readerName = readerName;
        book.returnDate = returnDate;
        book.reservedBy = null;
      } else if (status === 'reserved') {
        book.reservedBy = readerName;
      } else if (status === 'rejected') {
        book.status = 'taken';
        book.reservedBy = null;
      }
    }

    return writeFile(this.datafile, JSON.stringify(data));
  }
}

module.exports = BooksService;
