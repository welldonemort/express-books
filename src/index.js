/* eslint-disable no-console */
const express = require('express');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const path = require('path');

const BooksService = require('./services/BooksService');
const BookshelvesService = require('./services/BookshelvesService');

const booksService = new BooksService(path.join(__dirname, './data/books.json'));
const bookshelvesService = new BookshelvesService(path.join(__dirname, './data/bookshelves.json'));

const routes = require('./routes');

const app = express();
const port = 3000;

app.set('trust proxy', 1);
app.use(
  cookieSession({
    name: 'session',
    keys: ['SAmdaask442!', 'slSLDkdsakd13'],
  })
);

// для парсинга JSON
app.use(bodyParser.json());

app.use(
  '/api',
  routes({
    booksService,
    bookshelvesService,
  })
);

app.use((req, _, next) => next('404 No data'));

app.use((err, _, resp) => {
  console.error(err);
  const status = err.status || 500;
  return resp.status(status);
});

app.listen(port, () => console.log(`Express server listening on port ${port}!`));
