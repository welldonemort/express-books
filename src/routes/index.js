/* eslint-disable no-console */
const express = require('express');

const AVAILABLE_FILTERS = ['isbn', 'name', 'genre', 'author'];
const CREATE_BOOK_PARAMS = ['isbn', 'name', 'genre', 'author', 'bookshelfId'];

const router = express.Router();
const bookshelvesRoute = require('./bookshelves');

module.exports = (params) => {
  const { booksService, bookshelvesService } = params;

  // 1.	Searching for a book
  router.get('/books', async (req, resp, next) => {
    try {
      let books = await booksService.getList();

      const queries = req.query;
      const keys = Object.keys(queries);

      // 400
      if (!keys.includes('name') && !keys.includes('isbn')) {
        return resp.status(400).json({ message: 'NAME or ISBN filters are required!' });
      }

      books = books.filter((book) =>
        keys.every((key) => {
          const filterValue = queries[key] && queries[key].toLowerCase();
          const bookValue = book[key] && book[key].toLowerCase();

          return (
            !filterValue || !AVAILABLE_FILTERS.includes(key) || bookValue.includes(filterValue)
          );
        })
      );

      // 200
      if (books.length) {
        return resp.json({ books });
      }
      // 404
      return resp
        .status(404)
        .json({ message: 'There is no books with the provided parameters!', books });
    } catch (error) {
      // 500
      console.error(error);
      return next(error);
    }
  });

  // 2.	A librarian should be able to put a new book on a shelf
  router.post('/books', async (req, resp, next) => {
    try {
      const newBook = req.body;

      // 400
      // eslint-disable-next-line consistent-return
      CREATE_BOOK_PARAMS.forEach((key) => {
        if (!newBook[key]) {
          return resp.status(400).json({ message: `${key.toUpperCase()} param is required!` });
        }
      });

      const bookshelf = await bookshelvesService.getEntry(newBook.bookshelfId);

      // 404
      if (!bookshelf) {
        return resp
          .status(404)
          .json({ message: 'There is no bookshelf with the provided bookshelfId.' });
      }

      await booksService.addEntry(newBook);
      const books = await booksService.getList();

      // 200
      return resp.json({
        message: 'The new book was successfully placed on a bookshelf.',
        book: books[books.length - 1],
      });
    } catch (error) {
      // 500
      return next(error);
    }
  });

  // 3.	A librarian should be able to move a book between shelves
  router.post('/books/:id/move', async (req, resp, next) => {
    try {
      const { id } = req.params;
      const { bookshelfId } = req.body;

      // 400
      if (!bookshelfId) {
        return resp.status(400).json({ message: 'bookshelfId param is required!' });
      }

      const book = await booksService.getEntry(Number(id));

      // 404
      if (!book) {
        return resp.status(404).json({ message: 'There is no books with the provided ID!' });
      }

      await booksService.changeBookshelf(Number(id), bookshelfId);
      return resp.json({
        message: 'The book successfully placed on a bookshelf',
        book: { ...book, bookshelfId },
      });
    } catch (error) {
      // 500
      console.error(error);
      return next(error);
    }
  });

  // 4.	A librarian should be able to get the information on a book using its ID
  router.get('/books/:id', async (req, resp, next) => {
    try {
      const { id } = req.params;

      const book = await booksService.getEntry(Number(id));

      if (book) {
        return resp.json({ message: 'Successfully got all the information about the book.', book });
      }
      return resp.status(404).json({ message: 'There is no books with the provided ID!' });
    } catch (error) {
      // 500
      console.error(error);
      return next(error);
    }
  });

  // 5.	Taking a book
  // 6.	Reserving a book
  router.put('/books/:id', async (req, resp, next) => {
    try {
      const { id } = req.params;
      const { status, readerName, returnDate } = req.body;

      // 400
      if (!status) {
        return resp.status(400).json({ message: 'status param is required!' });
      }
      if (status === 'taken' && (!readerName || !returnDate)) {
        return resp.status(400).json({ message: 'readerName & returnDate params are required!' });
      }
      if (status === 'reserved' && !readerName) {
        return resp.status(400).json({ message: 'readerName param is required!' });
      }

      let book = await booksService.getEntry(Number(id));

      // 404
      if (status === 'reserved' && book.status === 'reserved') {
        return resp
          .status(404)
          .json({ message: 'This book is already reserved! Please try next time.' });
      }

      await booksService.updateBook({ id: Number(id), status, readerName, returnDate });
      book = await booksService.getEntry(Number(id));

      return resp.json({ message: `The book was successfully taken by ${readerName}`, book });
    } catch (error) {
      // 500
      console.error(error);
      return next(error);
    }
  });

  // 7.	Bookshelf information
  router.use('/bookshelves', bookshelvesRoute({ bookshelvesService }));

  return router;
};
