const express = require('express');

const router = express.Router();

module.exports = (params) => {
  const { bookshelvesService } = params;

  // 7.	Bookshelf information
  router.get('/:id', async (req, resp, next) => {
    try {
      const { id } = req.params;

      const bookshelf = await bookshelvesService.getEntry(Number(id));

      if (bookshelf) {
        return resp.json({
          message: 'Successfully got all the information about the bookshelf.',
          bookshelf,
        });
      }
      // 404
      return resp.status(404).json({ message: 'There is no bookshelf with the provided ID!' });
    } catch (error) {
      // 500
      console.error(error);
      return next(error);
    }
  });

  return router;
};
