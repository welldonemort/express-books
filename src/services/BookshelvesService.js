const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

/**
 * Logic for reading and writing data
 */
class BookshelvesService {
  /**
   * Constructor
   * @param {*} datafile Path to a JSON file that contains the data
   */
  constructor(datafile) {
    this.datafile = datafile;
  }

  /**
   * Fetches data from the JSON file provided to the constructor
   */
  async getData() {
    const data = await readFile(this.datafile, 'utf8');
    if (!data) return [];
    return JSON.parse(data);
  }

  /**
   * Get bookshelf by ID
   * @param {*} id The ID of a bookshelf
   */
  async getEntry(id) {
    const data = (await this.getData()) || [];
    const bookshelf = data.find((b) => b.id === id);

    return bookshelf;
  }

  /**
   * Get all items
   */
  async getList() {
    const data = await this.getData();
    return data;
  }

  /**
   * Add a new item
   * @param {*} id The bookshelf id
   * @param {*} bookId The book id
   */
  async putBook(id, bookId) {
    const data = (await this.getData()) || [];
    const bookshelf = data.find((bs) => bs.id === id);

    if (!bookshelf.booksIds.includes(bookId)) {
      bookshelf.booksIds.push(bookId);
    }

    return writeFile(this.datafile, JSON.stringify(data));
  }

  /**
   * Remove item
   * @param {*} id The bookshelf id
   * @param {*} bookId The book id
   */
  async removeBook(id, bookId) {
    const data = (await this.getData()) || [];
    const bookshelf = data.find((bs) => bs.id === id);

    if (bookshelf.booksIds.includes(bookId)) {
      const startIndex = bookshelf.booksIds.indexOf(bookId);
      bookshelf.booksIds.splice(startIndex, 1);
    }

    return writeFile(this.datafile, JSON.stringify(data));
  }
}

module.exports = BookshelvesService;
